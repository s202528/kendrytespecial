import PIL 
import os
from PIL import Image
import glob
def compressor(pic_dir,quality):
    new_dir = f"{pic_dir}_compressed_{quality}"
    os.makedirs(new_dir)
    print(os.listdir(pic_dir))
    for dirs in os.listdir(pic_dir):
        if os.path.isdir(f"{pic_dir}/{dirs}"):
            new_subdir = f"{new_dir}/{dirs}"
            os.makedirs(new_subdir)
            for pic in os.listdir(f"{pic_dir}/{dirs}"):
                img = Image.open(f"{pic_dir}/{dirs}/{pic}")
                img.save(f"{new_subdir}/{pic}",optimize=True,quality=quality)

compressor("flower_photos",20)
compressor("flower_photos",30)
compressor("flower_photos",40)
compressor("flower_photos",60)
compressor("flower_photos",80)