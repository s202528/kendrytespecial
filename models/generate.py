import numpy as np
import os
import PIL
import tempfile
import PIL.Image
import tensorflow as tf
import tensorflow_datasets as tfds
import pathlib
from tflite_model_maker.config import QuantizationConfig
from tensorflow.keras import layers

dataset_url = "https://storage.googleapis.com/download.tensorflow.org/example_images/flower_photos.tgz"
data_dir = tf.keras.utils.get_file(origin=dataset_url, 
                                   fname='flower_photos', 
                                   untar=True)
data_dir = pathlib.Path(data_dir)
print(data_dir)
image_count = len(list(data_dir.glob('*/*.jpg')))
print(image_count)
batch_size = 32
img_height = 180
img_width = 180
train_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="training",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)

val_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="validation",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)

class_names = train_ds.class_names
print(class_names)

AUTOTUNE = tf.data.AUTOTUNE

train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)
num_classes = 5

model1 = tf.keras.Sequential([
  layers.experimental.preprocessing.Rescaling(1./255),
  layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu'),
  layers.MaxPooling2D(pool_size=(2, 2)),
  layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu'),
  layers.MaxPooling2D(pool_size=(2, 2)),
  layers.Flatten(),
  layers.Dense(num_classes)
])
model1.compile(
  optimizer='adam',
  loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
  metrics=['accuracy'])


model2 = tf.keras.Sequential([
  layers.experimental.preprocessing.Rescaling(1./255),
  layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu'),
  layers.MaxPooling2D(pool_size=(2, 2)),
  layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu'),
  layers.MaxPooling2D(pool_size=(2, 2)),
  layers.Flatten(),
  layers.Dense(num_classes)
])
model2.compile(
  optimizer='adam',
  loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
  metrics=['accuracy'])


model3 = tf.keras.Sequential([
  layers.experimental.preprocessing.Rescaling(1./255),
  layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu'),
  layers.MaxPooling2D(pool_size=(2, 2)),
  layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu'),
  layers.MaxPooling2D(pool_size=(2, 2)),
  layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu'),
  layers.MaxPooling2D(pool_size=(2, 2)),
  layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu'),
  layers.MaxPooling2D(pool_size=(2, 2)),
  layers.Flatten(),
  layers.Dense(num_classes)
])
model3.compile(
  optimizer='adam',
  loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
  metrics=['accuracy'])


model4 = tf.keras.Sequential([
  layers.experimental.preprocessing.Rescaling(1./255),
  layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu'),
  layers.MaxPooling2D(pool_size=(2, 2)),
  layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu'),
  layers.MaxPooling2D(pool_size=(2, 2)),
  layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu'),
  layers.MaxPooling2D(pool_size=(2, 2)),
  layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu'),
  layers.MaxPooling2D(pool_size=(2, 2)),
  layers.Flatten(),
  layers.Dense(num_classes)
])
model4.compile(
  optimizer='adam',
  loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
  metrics=['accuracy'])


models = [model1,model2,model3, model4]
# class_names = val_ds.class_names
# for images, labels in val_ds:
#   for i in range(9):
#     print(class_names[labels[i]])
epochs = 5
for i in range(len(models)):
  models[i].fit(train_ds,epochs=epochs)
  loss,accuracy = model1.evaluate(val_ds, verbose=2)
  print(f'accuarcacy of model {i} is {loss,accuracy}')
  converter = tf.lite.TFLiteConverter.from_keras_model(models[i])
  tflite_model = converter.convert()

#Save the full model.
  with open(f'model{i}.tflite', 'wb') as f:
    f.write(tflite_model)


#save the tflite dynamic quant model
  converter.optimizations = [tf.lite.Optimize.DEFAULT]
  tflite_quant_model = converter.convert()
  tflite_model_quant_file = f"model_quant-{i}.tflite"
  with open(tflite_model_quant_file, 'wb') as f:
    f.write(tflite_quant_model)




#save the float 16 quant model
  converter.target_spec.supported_types = [tf.float16]
  tflite_fp16_model = converter.convert()
  tflite_model_fp16_file = f"model_quant_f16-{i}.tflite"
  with open(tflite_model_fp16_file, 'wb') as f:
    f.write(tflite_fp16_model)
