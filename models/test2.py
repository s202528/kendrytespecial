import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
# Load the TFLite model and allocate tensors.

from tflite_model_maker.image_classifier import DataLoader
batch_size = 32
img_height = 180
img_width = 180
val_ds = tf.keras.preprocessing.image_dataset_from_directory(
  "flower_photos",
  validation_split=0.2,
  subset="validation",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)
file = open("results.txt","a")
#test_image = val_ds[0]
#
#print(input_tensor_index)
class_names = val_ds.class_names
#plt.figure(figsize=(10, 10))
# for j in range(4):
#   interpreter = tf.lite.Interpreter(model_path=f"model{j}.tflite")

#   interpreter.allocate_tensors()
#   input_tensor_index = interpreter.get_input_details()[0]["index"]
#   output = interpreter.tensor(interpreter.get_output_details()[0]["index"])
#   prediction_digits = []
#   actual_labels = []
#   for images, labels in val_ds:
#     #print(len(images))
#     for i in range(len(images)):   
#       test_image = np.expand_dims(images[i], axis=0).astype(np.float32)
#       interpreter.set_tensor(input_tensor_index, test_image)
#       #print(f"invoking interpreter{i/len(images)}")
#       interpreter.invoke()
#       digit = np.argmax(output()[0])
#     #print(class_names[digit])
#     #print(class_names[labels[i]])
#       actual_labels.append(class_names[labels[i]])
#       prediction_digits.append(class_names[digit])
#   accurate_count = 0
#   for index in range(len(prediction_digits)):
#     if prediction_digits[index] == actual_labels[index]:
#       accurate_count += 1
#     accuracy = float(accurate_count) / len(prediction_digits)
#   file.writelines(f'Accuracy of model model{j}.tflite is {accuracy}')
#   print(f'Accuracy of model model{j}.tflite is {accuracy}')

# for j in range(4):
#   interpreter = tf.lite.Interpreter(model_path=f"model_quant-{j}.tflite")

#   interpreter.allocate_tensors()
#   input_tensor_index = interpreter.get_input_details()[0]["index"]
#   output = interpreter.tensor(interpreter.get_output_details()[0]["index"])
#   prediction_digits = []
#   actual_labels = []
#   for images, labels in val_ds:
#     #print(len(images))
#     for i in range(len(images)):   
#       test_image = np.expand_dims(images[i], axis=0).astype(np.float32)
#       interpreter.set_tensor(input_tensor_index, test_image)
#       print(f"invoking interpreter{i/len(images)}")
#       interpreter.invoke()
#       digit = np.argmax(output()[0])
#     #print(class_names[digit])
#     #print(class_names[labels[i]])
#       actual_labels.append(class_names[labels[i]])
#       prediction_digits.append(class_names[digit])
#   accurate_count = 0
#   for index in range(len(prediction_digits)):
#     if prediction_digits[index] == actual_labels[index]:
#       accurate_count += 1
#     accuracy = float(accurate_count) / len(prediction_digits)
#   file.writelines(f'Accuracy of model model_quant-{j}.tflite is {accuracy}')
#   print(f'Accuracy of model model_quant-{j}.tflite is {accuracy}')

for j in range(4):
  interpreter = tf.lite.Interpreter(model_path=f"model_quant_f16-{j}.tflite")

  interpreter.allocate_tensors()
  input_tensor_index = interpreter.get_input_details()[0]["index"]
  output = interpreter.tensor(interpreter.get_output_details()[0]["index"])
  prediction_digits = []
  actual_labels = []
  for images, labels in val_ds:
    #print(len(images))
    for i in range(len(images)):   
      test_image = np.expand_dims(images[i], axis=0).astype(np.float32)
      interpreter.set_tensor(input_tensor_index, test_image)
      interpreter.invoke()
      digit = np.argmax(output()[0])
    #print(class_names[digit])
    #print(class_names[labels[i]])
      actual_labels.append(class_names[labels[i]])
      prediction_digits.append(class_names[digit])
  accurate_count = 0
  for index in range(len(prediction_digits)):
    if prediction_digits[index] == actual_labels[index]:
      accurate_count += 1
    accuracy = float(accurate_count) / len(prediction_digits)
  print('writing to file')
  file.writelines(f'Accuracy of model model_quant_f16{j}.tflite is {accuracy} ')
  print(f'Accuracy of model model_quant_f16{j}.tflite is {accuracy}')
# Get input and output tensors.
# input_details = interpreter.get_input_details()
# output_details = interpreter.get_output_details()
# data = DataLoader.from_folder(image_path)
# # Test the model on random input data.
# input_shape = input_details[0]['shape']
# input_data = np.array(np.random.random_sample(input_shape), dtype=np.float32)
# interpreter.set_tensor(input_details[0]['index'], input_data)

# interpreter.invoke()

# # The function `get_tensor()` returns a copy of the tensor data.
# # Use `tensor()` in order to get a pointer to the tensor.
# output_data = interpreter.get_tensor(output_details[0]['index'])
# print(output_data)
file.close()