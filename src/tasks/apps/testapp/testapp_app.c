#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <plic.h>
#include <dvp.h>
#include <sysctl.h>
#include <kpu.h>

#include "../common.h"
#include "board_config.h"
#include "nt35310.h"
#include "w25qxx.h"
#include "lcd.h"
#include "ov2640.h"
#include "config.h"
#include "pconfig.h"

#include "board.h"
#include "gpio.h"
#include "timer.h"
#include "radio.h"




int8_t RssiValue = 0;
int8_t SnrValue = 0;


typedef enum
{
    LOWPOWER,
    RX,
    RX_TIMEOUT,
    RX_ERROR,
    TX,
    TX_TIMEOUT,
}States_t;

const uint8_t PingMsg[] = "PING";
const uint8_t PongMsg[] = "PONG";

uint16_t BufferSize = BUFFER_SIZE;
uint8_t Buffer[BUFFER_SIZE];

States_t State = RX;




/*!
 * Radio events function pointer
 */
static RadioEvents_t RadioEvents;

/*!
 * LED GPIO pins objects
 */

void OnTxDone( void );

/*!
 * \brief Function to be executed on Radio Rx Done event
 */
void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );

/*!
 * \brief Function executed on Radio Tx Timeout event
 */
void OnTxTimeout( void );

/*!
 * \brief Function executed on Radio Rx Timeout event
 */
void OnRxTimeout( void );

/*!
 * \brief Function executed on Radio Rx Error event
 */
void OnRxError( void );

// camera image data buffer
uint8_t* ai_image;
#define LORA_SPI_CHANNEL 1
#define LORA_SPI_CHIP 0

uint8_t* g_lcd_gram0;
uint8_t* g_lcd_gram1;

bool isMaster = true;
uint8_t i;
int milli_seconds = 0;
  
    // Storing start time

// ai result
uint8_t ai_person_count;

static void ram_draw_box(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t width, uint16_t color){

  uint16_t * lcd_gram = g_ram_mux ? g_lcd_gram0 : g_lcd_gram1;

  for(int x = x1; x < x2; x++){
    int x_ = x + (x%2 ? -1 : 1);
    lcd_gram[y1 * 320 + x_] = color;
  }
  for(int x = x1; x < x2; x++){
    int x_ = x + (x%2 ? -1 : 1);
    lcd_gram[y2 * 320 + x_] = color;
  }

  x1 += x1%2 ? -1 : 1;
  x2 += x2%2 ? -1 : 1;

  for(int y = y1; y < y2; y++){
    lcd_gram[y * 320 + x1] = color;
  }
  for(int y = y1; y < y2; y++){
    lcd_gram[y * 320 + x2] = color;
  }

}

static void drawboxes(uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint32_t class, float prob)
{
  if (x1 >= 320)
    x1 = 319;
  if (x2 >= 320)
    x2 = 319;
  if (y1 >= 240)
    y1 = 239;
  if (y2 >= 240)
    y2 = 239;

  lcd_draw_rectangle(x1, y1, x2, y2, 2, RED);
}
static void board_init()
{
  fpioa_set_function(SX1261_SPI_NSS_PIN, SX1261_SPI_NSS_FUNC);
  fpioa_set_function(SX1261_SPI_MOSI_PIN, SX1261_SPI_MOSI_FUNC);
  fpioa_set_function(SX1261_SPI_MISO_PIN, SX1261_SPI_MISO_FUNC);
  fpioa_set_function(SX1261_SPI_SCK_PIN, SX1261_SPI_SCK_FUNC);
  fpioa_set_function(SX1261_BUSY_PIN, SX1261_BUSY_FUNC);
  fpioa_set_function(SX1261_DIO1_PIN, SX1261_DIO1_FUNC);
  fpioa_set_function(SX1261_RESET_PIN, SX1261_RESET_FUNC);
  fpioa_set_function(SX1261_FEM_CRX_PIN, SX1261_FEM_CRX_FUNC);

  fpioa_set_function(LED_R_PIN, LED_R_FUNC);
  fpioa_set_function(LED_G_PIN, LED_G_FUNC);
  fpioa_set_function(LED_B_PIN, LED_B_FUNC);

  gpiohs_set_drive_mode(LED_R_GPIO, GPIO_DM_OUTPUT);
  gpiohs_set_drive_mode(LED_G_GPIO, GPIO_DM_OUTPUT);
  gpiohs_set_drive_mode(LED_B_GPIO, GPIO_DM_OUTPUT);

  gpiohs_set_pin(LED_R_GPIO, GPIO_PV_HIGH);
  gpiohs_set_pin(LED_G_GPIO, GPIO_PV_LOW);
  gpiohs_set_pin(LED_B_GPIO, GPIO_PV_LOW);


  SX126xIoInit();
  mxtimer_setup();

  spi_init(LORA_SPI_CHANNEL, SPI_WORK_MODE_0, SPI_FF_STANDARD, 8, 0);

  spi_set_clk_rate(LORA_SPI_CHANNEL, 1000 * 1000);
}
void ai_app_init(){
    g_lcd_gram0 = (uint32_t *)iomem_malloc(320 * 240 * 2);
    g_lcd_gram1 = (uint32_t *)iomem_malloc(320 * 240 * 2);
    ai_image = (uint8_t *)iomem_malloc(320 * 240 * 3);
   
    /* LCD init */
    printf("LCD init\r\n");
    lcd_init();
    lcd_set_direction(DIR_YX_LRDU);
    lcd_clear(BLACK);
    lcd_draw_string(150, 100, "We are here huoston", WHITE);
    
    /* DVP init */
    printf("DVP init\r\n");
    dvp_init(8);
    dvp_set_xclk_rate(24000000);
    dvp_enable_burst();
    dvp_set_output_enable(DVP_OUTPUT_AI, 1);
    dvp_set_output_enable(DVP_OUTPUT_DISPLAY, 1);
    dvp_set_image_format(DVP_CFG_RGB_FORMAT);
    dvp_set_image_size(320,240);
    ov2640_init();

    dvp_set_ai_addr((uint32_t)ai_image, (uint32_t)(ai_image + 320 * 240), (uint32_t)(ai_image + 320 * 240 * 2));
    dvp_config_interrupt(DVP_CFG_START_INT_ENABLE | DVP_CFG_FINISH_INT_ENABLE, 0);
    dvp_disable_auto();

    /* DVP interrupt config */
    printf("DVP interrupt config\r\n");
    plic_set_priority(IRQN_DVP_INTERRUPT, 1);
    plic_irq_register(IRQN_DVP_INTERRUPT, on_irq_dvp, NULL);
    plic_irq_enable(IRQN_DVP_INTERRUPT);

    /* enable global interrupt */
    sysctl_enable_irq();

    /* system start */
    printf("system start\r\n");
    g_dvp_finish_flag = 0;
    dvp_clear_interrupt(DVP_STS_FRAME_START | DVP_STS_FRAME_FINISH);
    dvp_config_interrupt(DVP_CFG_START_INT_ENABLE | DVP_CFG_FINISH_INT_ENABLE, 1);
    
     board_init();
    RadioEvents.TxDone = OnTxDone;
    RadioEvents.RxDone = OnRxDone;
    RadioEvents.TxTimeout = OnTxTimeout;
    RadioEvents.RxTimeout = OnRxTimeout;
    RadioEvents.RxError = OnRxError;

    Radio.Init( &RadioEvents );

    Radio.SetChannel( RF_FREQUENCY );
    Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
                                   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
                                   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
                                   true, 0, 0, LORA_IQ_INVERSION_ON, 10000 );

    Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
                                   LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                                   LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
                                   0, true, 0, 0, LORA_IQ_INVERSION_ON, true );

    Radio.SetMaxPayloadLength( MODEM_LORA, BUFFER_SIZE );
    
    printf("State is %c \n\r",State);
    fpioa_set_function(LED_R_PIN, LED_R_FUNC);
  fpioa_set_function(LED_G_PIN, LED_G_FUNC);
  fpioa_set_function(LED_B_PIN, LED_B_FUNC);
  gpiohs_set_drive_mode(LED_R_GPIO, GPIO_DM_OUTPUT);
  gpiohs_set_drive_mode(LED_G_GPIO, GPIO_DM_OUTPUT);
  gpiohs_set_drive_mode(LED_B_GPIO, GPIO_DM_OUTPUT);
  gpiohs_set_pin(LED_R_GPIO, GPIO_PV_LOW);
  gpiohs_set_pin(LED_G_GPIO, GPIO_PV_LOW);
  gpiohs_set_pin(LED_B_GPIO, GPIO_PV_LOW);
//Radio.Rx( RX_TIMEOUT_VALUE );
}

void ai_app_process(){
  //printf("In AI app process\r\n");
    /* dvp finish*/
  
   while (g_dvp_finish_flag == 0)
      ;
     if( Radio.IrqProcess != NULL )
            {
                Radio.IrqProcess( );
            }
    switch( State )
        {
        case RX:
            if( isMaster == true )
            {
                if( BufferSize > 0 )
                {
                    if( strncmp( ( const char* )Buffer, ( const char* )PongMsg, 4 ) == 0 )
                    {
                        // Indicates on a LED that the received frame is a PONG
                        gpiohs_set_pin(LED_R_GPIO, GPIO_PV_HIGH);
                        gpiohs_set_pin(LED_G_GPIO, GPIO_PV_LOW);
                        gpiohs_set_pin(LED_B_GPIO, GPIO_PV_LOW);

                        // Send the next PING frame
                        Buffer[0] = 'P';
                        Buffer[1] = 'I';
                        Buffer[2] = 'N';
                        Buffer[3] = 'G';
                        // We fill the buffer with numbers for the payload
                        for( i = 4; i < BufferSize; i++ )
                        {
                            Buffer[i] = i - 4;
                        }
                        DelayMs( 1 );
                        Radio.Send( Buffer, BufferSize );
                    }
                    else if( strncmp( ( const char* )Buffer, ( const char* )PingMsg, 4 ) == 0 )
                    { // A master already exists then become a slave
                        isMaster = false;
                        gpiohs_set_pin(LED_R_GPIO, GPIO_PV_LOW);
                        gpiohs_set_pin(LED_G_GPIO, GPIO_PV_LOW);
                        gpiohs_set_pin(LED_B_GPIO, GPIO_PV_LOW);
                        Radio.Rx( RX_TIMEOUT_VALUE );
                    }
                    else // valid reception but neither a PING or a PONG message
                    {    // Set device as master ans start again
                        isMaster = true;
                        Radio.Rx( RX_TIMEOUT_VALUE );
                    }
                }
            }
            else
            {
                if( BufferSize > 0 )
                {
                    if( strncmp( ( const char* )Buffer, ( const char* )PingMsg, 4 ) == 0 )
                    {
                        // Indicates on a LED that the received frame is a PING
                       gpiohs_set_pin(LED_R_GPIO, GPIO_PV_LOW);
                        gpiohs_set_pin(LED_G_GPIO, GPIO_PV_HIGH);
                        gpiohs_set_pin(LED_B_GPIO, GPIO_PV_LOW);

                        // Send the reply to the PONG string
                        Buffer[0] = 'P';
                        Buffer[1] = 'O';
                        Buffer[2] = 'N';
                        Buffer[3] = 'G';
                        // We fill the buffer with numbers for the payload
                        for( i = 4; i < BufferSize; i++ )
                        {
                            Buffer[i] = i - 4;
                        }
                        DelayMs( 1 );
                        Radio.Send( Buffer, BufferSize );
                    }
                    else // valid reception but not a PING as expected
                    {    // Set device as master and start again
                        isMaster = true;
                        Radio.Rx( RX_TIMEOUT_VALUE );
                    }
                }
            }
            State = LOWPOWER;
            break;
        case TX:
            // Indicates on a LED that we have sent a PING [Master]
            // Indicates on a LED that we have sent a PONG [Slave]
            
            Radio.Rx( RX_TIMEOUT_VALUE );
            State = LOWPOWER;
            break;
        case RX_TIMEOUT:
        case RX_ERROR:
            if( isMaster == true )
            {
                // Send the next PING frame
                Buffer[0] = 'P';
                Buffer[1] = 'I';
                Buffer[2] = 'N';
                Buffer[3] = 'G';
                for( i = 4; i < BufferSize; i++ )
                {
                    Buffer[i] = i - 4;
                }
                DelayMs( 1 );
                Radio.Send( Buffer, BufferSize );
            }
            else
            {
                Radio.Rx( RX_TIMEOUT_VALUE );
            }
            State = LOWPOWER;
            break;
        case TX_TIMEOUT:
            Radio.Rx( RX_TIMEOUT_VALUE );
            State = LOWPOWER;
            break;
        case LOWPOWER:
        default:
            // Set low power
            break;
        }
    float *output;
    size_t output_size;

    uint32_t * lcd_gram = g_ram_mux ? g_lcd_gram0 : g_lcd_gram1;
    g_ram_mux ^= 0x01;
    
    
    g_dvp_finish_flag = 0;
    

   // if( strncmp( ( const char* )Buffer, ( const char* )PingMsg, 4 ) == 0 ){
     // printf("Printing data in buffer \r\n");
      //printf("%c",Buffer[0]);
    //}
    


}

void lora_app_generate_payload(uint8_t* payload, uint8_t* payload_size){
     lcd_draw_string(150, 100, "We are here huoston", WHITE);
    *payload_size = 2;
    payload[0] = 10;

    if(ai_person_count == 1){
        gpiohs_set_pin(LED_R_GPIO, GPIO_PV_LOW);
        gpiohs_set_pin(LED_G_GPIO, GPIO_PV_HIGH);
        gpiohs_set_pin(LED_B_GPIO, GPIO_PV_LOW);
    }else if (ai_person_count == 0){
        gpiohs_set_pin(LED_R_GPIO, GPIO_PV_LOW);
        gpiohs_set_pin(LED_G_GPIO, GPIO_PV_LOW);
        gpiohs_set_pin(LED_B_GPIO, GPIO_PV_HIGH);
    }else{
        gpiohs_set_pin(LED_R_GPIO, GPIO_PV_HIGH);
        gpiohs_set_pin(LED_G_GPIO, GPIO_PV_HIGH);
        gpiohs_set_pin(LED_B_GPIO, GPIO_PV_LOW);
    }

}


void OnTxDone( void )
{
   lcd_clear(BLACK);
    lcd_draw_string(150, 100, "Tx done", WHITE);
    Radio.Sleep( );
    State = TX;
}

void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
   lcd_clear(BLACK);
    lcd_draw_string(150, 100, "Rx done", WHITE);
    Radio.Sleep( );
    BufferSize = size;
    memcpy( Buffer, payload, BufferSize );
    RssiValue = rssi;
    SnrValue = snr;
    State = RX;
}

void OnTxTimeout( void )
{
   lcd_clear(BLACK);
    lcd_draw_string(150, 100, "Tx Timeoutt", WHITE);
    Radio.Sleep( );
    State = TX_TIMEOUT;
}

void OnRxTimeout( void )
{
   lcd_clear(BLACK);
    lcd_draw_string(150, 100, "RX Timeoutt", WHITE);
    Radio.Sleep( );
    State = RX_TIMEOUT;
}

void OnRxError( void )
{
   lcd_clear(BLACK);
    lcd_draw_string(150, 100, "Rx Error", WHITE);
    Radio.Sleep( );
    State = RX_ERROR;
}